package JUnitTest;

import Polinom.Monom;
import Polinom.Operatii;
import Polinom.Polinom;
import junit.framework.Test;
import junit.framework.TestCase;

public class MyTest extends TestCase{
	Operatii op;
	Polinom p1;
	Polinom p2;
	public void setUp() {
		op=new Operatii();
		Monom m1= new Monom(2,3);
		Monom m2= new Monom(1,3);
		Monom m3= new Monom(0,3);//3x^2+3x+3
		p1=new Polinom();
		p1.add(m1);
		p1.add(m2);
		p1.add(m3);
		Monom m4= new Monom(1,4);//4x+2
		Monom m5= new Monom(0,2);
		p2=new Polinom();
		p2.add(m4);
		p2.add(m5);
			
	}
	
	public void testadd() {
		op.adunareSiScadere(p1, p2, 0);
		Polinom add=op.getRezultat();
		TestCase.assertEquals("+3x^2+7x+5", add.toString());
	}
	public void testsub() {
		op.adunareSiScadere(p1, p2, 1);
		Polinom sub=op.getRezultat();
		TestCase.assertEquals("+3x^2-x+1", sub.toString());
	}
	public void testmul() {
		op.inmultire(p1, p2);
		Polinom mul=op.getRezultat();
		TestCase.assertEquals("+12x^3+18x^2+18x+6", mul.toString());
	}
	public void testdiv(){
		op.impartire(p1, p2);
		String rez=op.getRezultat().toString()+" rest "+op.getRest().toString();
		TestCase.assertEquals("+0.75x+0.38 rest +2.25",rez );
	}
	public void testder() {
		op.derivare(p1);
		Polinom der=op.getRezultat();
		TestCase.assertEquals("+6x+3", der.toString());
	}
	public void testint() {
		op.integrare(p1);
		Polinom inte=op.getRezultat();
		TestCase.assertEquals("+x^3+1.50x^2+3x+c", inte.toString()+"+c");
	}

}
