package MVC;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import Polinom.Monom;
import Polinom.Operatii;
import Polinom.Polinom;

public class Main {
	public static void main(String[] args)
	{
		
	PModel model=new PModel();
	PView view=new PView(model);
	PController controller=new PController(view, model);
	}

}
