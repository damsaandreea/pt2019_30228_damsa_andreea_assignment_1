package MVC;

import Polinom.Operatii;
import Polinom.Polinom;

public class PModel {
	private Polinom p1;
	private Polinom p2;
	private String rest;
	private String rs;
	
	public String plus(String s1, String s2) {
		 p1=Polinom.parse(s1);
		 p2=Polinom.parse(s2);
		Operatii o=new Operatii();
		o.adunareSiScadere(p1, p2, 0);
		rs=o.getRezultat().toString();
		return rs;
	}
	public String minus(String s1, String s2) {
		 p1=Polinom.parse(s1);
		 p2=Polinom.parse(s2);
		Operatii o=new Operatii();
		o.adunareSiScadere(p1, p2, 1);
		rs=o.getRezultat().toString();
		return rs;
	}
	public String ori(String s1, String s2) {
		p1=Polinom.parse(s1);
		 p2=Polinom.parse(s2);
		Operatii o=new Operatii();
		o.inmultire(p1, p2);
		rs=o.getRezultat().toString();
		return rs;
	}
	public String[] div(String s1, String s2) {
		p1=Polinom.parse(s1);
		p2=Polinom.parse(s2);
		Operatii o=new Operatii();
		o.impartire(p1, p2);
		rs=o.getRezultat().toString();
		rest=o.getRest().toString();
		String[] v= new String[2];
		v[0]=rs;
		v[1]=rest;
		return v;
	}
	public String der(String s) {
		p1=Polinom.parse(s);
		Operatii o=new Operatii();
		o.derivare(p1);
		rs=o.getRezultat().toString();
		return rs;
	}
	public String inte(String s) {
		p1=Polinom.parse(s);
		Operatii o= new Operatii();
		o.integrare(p1);
		rs=o.getRezultat().toString();
		return rs;
	}
	

}
