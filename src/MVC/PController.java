package MVC;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

public class PController {
		private PView view;
		private PModel model;
	public PController(PView view, PModel model)
	{
		this.view=view;
		this.model=model;
		
		view.plusButtonListener(new PlusListener());
		view.minusButtonListener(new MinusListener());
		view.oriButtonListener(new OriListener());
		view.inteButtonListener(new InteListener());
		view.impButtonListener(new ImpListener());
		view.derButtonListener(new DerListener());
	}
	class PlusListener implements ActionListener{
		public void actionPerformed(ActionEvent a)
		{
			try {
			String ui1="";
			String ui2="";
			   ui1=view.getT1();
			   ui2=view.getT2();
			String rez=model.plus(ui1, ui2);
			view.setTr(rez);
			}
			catch(Exception e) {
				JOptionPane.showMessageDialog(null, "Reintroduceti polinomul!");
		}
	}
	}
	class MinusListener implements ActionListener{
		public void actionPerformed(ActionEvent m)
		{
			try {
			String ui1="";
			String ui2="";
			   ui1=view.getT1();
			   ui2=view.getT2();
			String rez=model.minus(ui1, ui2);
			view.setTr(rez);
			}
			catch(Exception e) {
				JOptionPane.showMessageDialog(null, "Reintroduceti polinomul!");
			}
		}
	}
	
	class OriListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			try {
			String ui1="";
			String ui2="";
			   ui1=view.getT1();
			   ui2=view.getT2();
			String rez=model.ori(ui1, ui2);
			view.setTr(rez);
			}
			catch(Exception exe) {
				JOptionPane.showMessageDialog(null, "Reintroduceti polinomul!");
			}
		}
	}
	
	class ImpListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			try {
			String ui1="";
			String ui2="";
			   ui1=view.getT1();
			   ui2=view.getT2();
			String[] rez=model.div(ui1, ui2);
			String ret="";
			if (rez[0]=="")
				ret="0";
			  else
			    ret=rez[0];
			if (!rez[1].equals(""))
				ret+=" rest "+rez[1];
			System.out.println(rez[0]+" "+rez[1]);
			view.setTr(ret);}
			catch(Exception exe) {
				JOptionPane.showMessageDialog(null, "Reintroduceti polinomul!");}
			}
	}
	
	class DerListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			try {
			String s="";
			Enumeration<AbstractButton> allRadioButton=view.g.getElements();  
	        while(allRadioButton.hasMoreElements())  
	        {  
	           JRadioButton temp=(JRadioButton)allRadioButton.nextElement();  
	           if(temp.isSelected()) {
	        	    s=temp.getText();
	           }
		}
	        String rez="";
	        if (s.equals("Polinom1:")) {
	        	String ui1="";
	        	ui1=view.getT1();
	        	 rez=model.der(ui1);
	        }
	        else if (s.equals("Polinom2:")) {
	        	String ui2="";
	        	ui2=view.getT2();
	        	 rez=model.der(ui2);
	        }
	        else if (s.equals("Rezultat:")) {
	        	String in="";
	        	in=view.getTr();
	        	 rez=model.der(in);
	        }
	        else if (s.equals("")) {
	        	String msj=new String("Selectati un polinom!");
	        	view.showMessage(msj);
	        }
	        view.setTr(rez);}
			catch(Exception exe) {
				JOptionPane.showMessageDialog(null, "Reintroduceti polinomul!");}
	}
	
}
	class InteListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			try {
			String s="";
			Enumeration<AbstractButton> allRadioButton=view.g.getElements();  
	        while(allRadioButton.hasMoreElements())  
	        {  
	           JRadioButton temp=(JRadioButton)allRadioButton.nextElement();  
	           if(temp.isSelected()) {
	        	    s=temp.getText();
	           }
		}
	        String rez="";
	        if (s.equals("Polinom1:")) {
	        	String ui1="";
	        	ui1=view.getT1();
	        	 rez=model.inte(ui1);
	        }
	        else if (s.equals("Polinom2:")) {
	        	String ui2="";
	        	ui2=view.getT2();
	        	 rez=model.inte(ui2);
	        }
	        else if (s.equals("Rezultat:")) {
	        	String in="";
	        	in=view.getTr();
	        	 rez=model.inte(in);
	        }
	        else if (s.equals("")) {
	        	String msj=new String("Selectati un polinom!");
	        	view.showMessage(msj);
	        }
	        rez+="+c";
	        view.setTr(rez);}
			catch(Exception exe) {
				JOptionPane.showMessageDialog(null, "Reintroduceti polinomul!");}
		}
	}
	
}
