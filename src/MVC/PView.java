package MVC;
import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;
//aici definesc elementele grafice, scriu metode de atribuire ascultatori elementelor
//si metode de get si set pt campuri text;
public class PView extends JFrame{
	private PModel model;
	private JRadioButton p1= new JRadioButton("Polinom1:");
	private JRadioButton p2= new JRadioButton("Polinom2:");
	private JRadioButton r= new JRadioButton("Rezultat:");
	private JLabel info=new JLabel("Alege un polinom pentru una dintre urmatoarele operatii:");
	private JTextField t1=new JTextField(25);
	private JTextField t2=new JTextField(25);
	private JTextField tr=new JTextField(25);
	private JButton ad=new JButton("Adunare");
	private JButton sc=new JButton("Scadere");
	private JButton in=new JButton("Inmultire");
	private JButton im=new JButton("Impartire");
	private JButton de=new JButton("Derivare");
	private JButton inte= new JButton("Integrare");
	public ButtonGroup g=new ButtonGroup();
	public PView(PModel model) {
		this.model=model;
		g.add(p1);
		g.add(p2);
		g.add(r);
		JPanel content1=new JPanel();
		content1.add(p1);
		content1.add(t1);
		content1.setLayout(new FlowLayout());
		JPanel content2=new JPanel();
		content2.add(p2);
		content2.add(t2);
		content2.setLayout(new FlowLayout());
		JPanel c3=new JPanel();
		c3.add(content1);
		c3.add(content2);
		c3.setLayout(new BoxLayout(c3,BoxLayout.Y_AXIS));
		JPanel op=new JPanel();
		op.add(ad);
		op.add(sc);
		op.add(in);
		op.add(im);
		op.setLayout(new BoxLayout(op, BoxLayout.Y_AXIS));
		JPanel sus= new JPanel();
		sus.add(c3);
		sus.add(op);
		sus.setLayout(new FlowLayout());
		JPanel mijl=new JPanel();
		mijl.add(r);
		mijl.add(tr);
		mijl.setLayout(new FlowLayout());
		mijl.setSize(new Dimension(50,100));;
		JPanel jos= new JPanel();
		jos.add(new JLabel("Pentru operatiile urmatoare selectati un polinom"));
		jos.add(de);
		jos.add(inte);
		jos.setLayout(new BoxLayout(jos,BoxLayout.Y_AXIS));
		JPanel j=new JPanel();
		j.add(mijl);
		j.add(jos);
		j.setLayout(new BoxLayout(j,BoxLayout.Y_AXIS));
		j.setSize(new Dimension(50,100));
		JPanel fin=new JPanel();
		fin.add(sus);
		fin.add(j);
		fin.setLayout(new BoxLayout(fin,BoxLayout.Y_AXIS));
		this.setContentPane(fin);
		this.setVisible(true); 
		this.setSize(600, 300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Calculator");
		}
	public void plusButtonListener(ActionListener aal)
	{
		ad.addActionListener(aal);
	}
	
	public void minusButtonListener(ActionListener mal)
	{
		sc.addActionListener(mal);
	}
	
	public void oriButtonListener(ActionListener oal)
	{
		in.addActionListener(oal);
	}
	public void impButtonListener(ActionListener ial)
	{
		im.addActionListener(ial);
	}
	public void derButtonListener(ActionListener dal)
	{
		de.addActionListener(dal);
	}
	public void inteButtonListener(ActionListener inal)
	{
		inte.addActionListener(inal);
	}
	//setere
	public void setT1(String s)
	{
		t1.setText(s);
	}
	
	public void setT2(String s)
	{
		t2.setText(s);
	}
	
	public void setTr(String s)
	{
		tr.setText(s);
	}
	//getere
	public String getT1()
	{
		return t1.getText();
	}
	public String getT2() {
		return t2.getText();
	}
	
	public String  getTr() {
		return tr.getText();
	}
	public void showMessage(String errMessage) {
        JOptionPane.showMessageDialog(this, errMessage);	
	}
}
