package Polinom;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JOptionPane;

public class Polinom {
		ArrayList<Monom> lista;
	public Polinom() {
		lista=new ArrayList<Monom>();
	}
	public void add(Monom m)
	{
		lista.add(m);
	}
	public void setLista(ArrayList<Monom> p)
	{
		this.lista=p;
	}
	public ArrayList<Monom> getLista()
	{
		return lista;
	}
	
	public int gradPolinom() {
		int i=0;
		int m=lista.size();
		while (i<m && lista.get(i).getCoef()==0 ) {
			i++;
		}
		if (i==m)
			return lista.get(i-1).getGrad();
		return lista.get(i).getGrad();
	}
	//returneaza coeficienntul monomului cu cel mai mare grad nenul, sau 0 daca nu exista
	public double coeficientNenul() {
		int i=0;
		int m=lista.size();
		if (lista.size()==1)
				return  lista.get(i).getCoef();		
		while (i<m && lista.get(i).getCoef()==0 ) {
			i++;
		}
		return lista.get(i).getCoef();		
	}
	public Polinom negare()
	{
		Polinom p=new Polinom();
		for (Monom it:lista)
		{
			double newc=(-1)*it.getCoef();
			Monom m1=new Monom(it.getGrad(), newc);
			p.add(m1);
		}
		return p;
	}// returneaza un monom cu coeficientii negati
	
	public String toString()
	{
		String r="";int first=0;
		if (this.gradPolinom()==0 && this.coeficientNenul()==0)
			{r+="0";
			return r;
			}
		ListIterator<Monom> iterator=lista.listIterator();
		while (iterator.hasNext()) 
			{
				Monom o=iterator.next();
				r+=o.toString();
			}
		return r;
}
	public static Polinom parse(String s){
		Polinom p=new Polinom();
		int grad=0,coef=0;
		String[] tokens=s.split("(?=\\+|\\-)"); 
		
		for (String it : tokens) {
		    int pozx=it.indexOf('x');
			if (pozx!=-1) {
				String sgrad,scoef=it.substring(0,pozx);
				if (pozx==it.length()-1)
					sgrad="1";
				else sgrad=it.substring(pozx+2);
				if (scoef.length()==0)
					coef=1;
				    else if (scoef.charAt(0)=='+' && scoef.length()==1)
				    	coef=1;
					else if (scoef.charAt(0)=='-' && scoef.length()==1)
						coef=-1;
					else
						coef=Integer.parseInt(scoef);
				if (sgrad.length()>0)
					grad=Integer.parseInt(sgrad);
				else grad=1;
				}
			else {//termen liber
					grad=0;
					coef=Integer.parseInt(it);
			      }
			Monom mono=new Monom(grad, coef);
			p.add(mono);
		   }
		
		return p;
		
		
		}
	
}
