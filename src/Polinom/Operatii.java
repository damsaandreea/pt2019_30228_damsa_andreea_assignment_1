package Polinom;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JOptionPane;

public class Operatii {
	private Polinom rezultat=new Polinom();
	private Polinom rest=new Polinom();
	public Polinom getRezultat() {
		return rezultat;
	}
	public Polinom getRest() {
		return this.rest;
	}

	public void setRezultat(Polinom rezultat) {
		this.rezultat = rezultat;
	}
    public void adunareSiScadere(Polinom p1, Polinom p2, int ident)
	{//ca sa nu repet prea mult cod->0-adunare; 1->scadere
		int l1=p1.getLista().size(), l2=p2.getLista().size();
    	ArrayList<Monom> lista1=p1.getLista(); 
    	ArrayList<Monom> lista2;
    			if (ident==0)
    			lista2=p2.getLista();
    			else {
    				lista2=p2.negare().getLista();
    				//pentru scadere fac adunare cu al doilea polinom negat
    			}
    	int c1=0, c2=0;// parcurgere liste cu 2 contoare
    	while (c1!=l1 && c2!=l2){
    		int grad1=lista1.get(c1).getGrad(), grad2=lista2.get(c2).getGrad();
    		if ( grad1>grad2 ){
    			rezultat.add(lista1.get(c1));
    			c1++;
    		}
    		else if (grad1 < grad2){
    			rezultat.add(lista2.get(c2));
    			c2++;
    		}
    		else {
    			 double coef=lista1.get(c1).getCoef()+lista2.get(c2).getCoef();
    			 Monom newMonom = new Monom(grad1, coef);
    			 rezultat.add(newMonom);
    			 c1++; c2++;
    		}
    	}
    	while (c1!=l1){
    		rezultat.add(lista1.get(c1));
			c1++;
    	}
    	while (c2!=l2){
    		rezultat.add(lista2.get(c2));
			c2++;
    	}
    	//un fel de interclasare, unde adun coeficientii monoamelor cu grad egal
    	//interclasare->pentru a mentine ordinea de la cel mai mare grad la cel mai mic
	}
    
    public void inmultire( Polinom p1, Polinom p2)
    {
    	for (Monom m:p1.getLista()) {
    		Polinom inter=m.mulCuPolinom(p2);
    		Operatii o= new Operatii();
    		o.adunareSiScadere(rezultat, inter, 0);
    		rezultat=o.getRezultat();
    	}
    }
    
    public void impartire(Polinom p1, Polinom p2)
    {
    	Polinom d=new Polinom();
    	d=p1;
    	if (p1.coeficientNenul()==0) {
    		Monom m1=new Monom(0,0);
    		rezultat.add(m1);
    		return;
    	}
    	if (p1.gradPolinom()==0 && p2.gradPolinom()==0) {
    		Monom m1=new Monom(0,p1.coeficientNenul()/p2.coeficientNenul());
    		rezultat.add(m1);
    		return;
    	}
    	if (p1.gradPolinom()==0) {
    		Polinom inter=new Polinom();
    		for (Monom m:p2.getLista()) {
    			Monom m1=new Monom(m.getGrad(), p1.coeficientNenul()/m.getCoef());
    			rezultat.add(m1);
    			return;
    		}
    	}
    	if (p2.gradPolinom()==0) {
    		Polinom inter=new Polinom();
    		for (Monom m:p1.getLista()) {
    			Monom m1=new Monom(m.getGrad(), m.getCoef()/p2.coeficientNenul());
    			rezultat.add(m1);
    			return;
    		}
    	}
    	if (d.gradPolinom() < p2.gradPolinom()){
    		rest=p1;
    		Monom m= new Monom(0,0);
    		Polinom r=new Polinom();
    		r.add(m);
    		rezultat=r;
    		return;
    	}
    	while (d.gradPolinom() >= p2.gradPolinom())
    	{
    		//algoritm de impartire
    		Polinom inter=new Polinom();
    		Monom m=new Monom(d.gradPolinom()-p2.gradPolinom(), d.coeficientNenul()/p2.coeficientNenul());
    		rezultat.add(m);
    		inter=m.mulCuPolinom(p2);
    		Operatii o=new Operatii();
    		o.adunareSiScadere(d, inter, 1);// fac sa scada, in loc sa aduna cu (-1)*inter
    		d=o.getRezultat();    	
    	}
    	rest=d; 
     }
    public void derivare(Polinom p) {
    	for (Monom m:p.getLista()) {
    		int grad=m.getGrad();
    		double coef=m.getCoef();
    		Monom n=new Monom(grad-1, grad*coef);
    		rezultat.add(n);
    	}
    }
    
    public void integrare(Polinom p) {
    	for (Monom m: p.getLista()) {
    		int grad=m.getGrad();
    		double coef=m.getCoef();
    		Monom m1= new Monom(grad+1,coef/(grad+1)*1.0);
    		rezultat.add(m1);
    	}
    }
}
