package Polinom;

public class Monom {
		private int grad;
		private double coef;
		public Monom(int grad, double coef) {
			this.grad=grad;
			this.coef=coef;
		}
		public String toString() {
			String r="";
			String c="";
			if (coef== (int)coef) {
				c=String.format("%d", (int)coef);
			}else {
				c=String.format("%.2f", coef);
			}
			if (coef==0)
				return r;
			if (coef>0)
				r+="+";
			if (coef!=1 && coef!=-1 )
				r+=c;
				else if (coef==1 && grad==0)
					r+="1";
			  else if (coef==-1 && grad==0)
				  r+="-1";
			  else if (coef==-1)
				  r+="-";
			if (grad==1)
				r+="x";
			else if (grad>1) {
				r+="x^"+grad;
			}
			return r;
		}
		public int getGrad() {
			return this.grad;
		}
		public void setGrad(int grad) {
			this.grad = grad;
		}
		public double getCoef() {
			return this.coef;
		}
		public void setCoef(int coef) {
			this.coef = coef;
		}
		//inmultire monom cu polinom
		public Polinom mulCuPolinom(Polinom p)
		{
			Polinom rez= new Polinom();
			for (Monom m:p.getLista())
			{
				int grad=this.grad+m.getGrad();
				double coef=this.coef*m.getCoef();
				Monom m1=new Monom(grad, coef);
				rez.add(m1);
			}
			return rez;
		}
		
}
